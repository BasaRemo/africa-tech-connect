require('dotenv').config()
const config = require('./config/SiteConfig')

const pathPrefix = config.pathPrefix === '/' ? '' : config.pathPrefix


const query = `{
  allContentfulEvent(sort: {fields: [startDate], order: ASC}) {
    edges {
      node {
        title
        startDate
        countryIso
        address
        url
        id
      }
    }
    totalCount
  }
}`;


const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const queries = [
  {
    query,
    transformer: ({ data }) => data.allContentfulEvent.edges.map(({ node }) => {
        var eventDate = new Date(node.startDate);
        node.month = eventDate.getMonth();
        node.unixTime = Date.parse(node.startDate)/1000;
        node.objectID = node.id
        return node
      }
    ),
  },
];

module.exports = {
  siteMetadata: {
    title: `Africa Tech Square`,
    siteUrl: config.siteUrl + pathPrefix,
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/events`,
      }
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.CONTENTFUL_SPACE_ID || '',
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN || '',
      }
    },
    {
      resolve: `gatsby-plugin-algolia`,
      options: {
        appId: process.env.ALGOLIA_APP_ID || '',
        apiKey: process.env.ALGOLIA_ADMIN_API_KEY || '',
        indexName: process.env.ALGOLIA_INDEX_NAME || '',
        queries,
        chunkSize: 10000, // default: 1000
      }
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-netlify`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_TRACKING_ID || '',
        // Setting this parameter is optional
        anonymize: false,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: config.siteTitle,
        short_name: config.siteTitleAlt,
        description: config.siteDescription,
        start_url: config.pathPrefix,
        background_color: config.backgroundColor,
        theme_color: config.themeColor,
        display: 'minimal-ui',
        icons: [
          {
            src: '/logos/logo-android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: '/logos/logo-android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },
    `gatsby-plugin-offline`,
  ],
}
