module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "portfolio"
  siteTitle: 'AfricaTechSquare', // Navigation and Site Title
  siteTitleAlt: `AfricaTechSquare - Where Africa's Tech connects`, // Alternative Site title for SEO
  siteUrl: 'https://www.africatechsquare.com', // Domain of your site. No trailing slash!
  siteLanguage: 'en', // Language Tag on <html> element
  siteLogo: '/logos/logo-1024.png', // Used for SEO and manifest
  siteDescription: `#1 portal to discover Tech Events and Conferences happening in Africa and in the African diaspora.
          We connect the African Tech Community in the Mainland and in the Diaspora by curating every year a list of the top events happening
          across the world in the Afrotech community.`,
  siteFBAppID: '123456789', // Facebook App ID
  userTwitter: 'africatechsquare', // Twitter Username
  // og: Meta Tags
  ogSiteName: 'africatechsquare', // Facebook Site Name
  copyright: 'Copyright © 2017. All rights reserved. Made with by ❤️ Basa', // Copyright in the footer of the site
  // You can translate these three words into your language if you want. They'll be shown on the project page header
  client: 'Client',
  date: 'Date',
  service: 'Service',
  // Date format used in your project header
  // More information here: https://date-fns.org/v1.29.0/docs/format
  dateFormat: 'DD.MM.YYYY',
  // Manifest and Progress color
  themeColor: '#3498DB',
  backgroundColor: '#2b2e3c',
  // Settings for typography.js
  headerFontFamily: 'Merriweather',
  bodyFontFamily: 'Roboto',
  baseFontSize: '16px'
}