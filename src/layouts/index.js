import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Favicon from 'react-favicon';

import styled from 'styled-components'

import 'uikit/dist/css/uikit.min.css';
import './index.css'
import 'react-instantsearch-theme-algolia/style.min.css'

import {InstantSearch} from 'react-instantsearch/dom';
import CustomNavBar from '../Components/CustomNavBar.js'
import Footer from '../Components/Footer.js'
import favicon from '../images/favicon.ico'

import {IntlProvider, FormattedDate} from 'react-intl';

const appId = 'YN6ZB5YGKP'//process.env.ALGOLIA_APP_ID || ''
const apiKey = 'e712af0f452796b5b08b38429443a49d'//process.env.ALGOLIA_API_KEY || ''
const indexName = 'africatechsquare'//process.env.ALGOLIA_INDEX_NAME || ''

const AppStyles = styled.div`
  a {
    color: #126AD1;
    font-weight: normal;
  }

  img {
    max-width: 100%;
  }
  
  background-color:#F8F8F8;
`

const TemplateWrapper = ({ children, data }) => (
  <IntlProvider locale="en">
    <div>
      <Favicon url={favicon} />
      <Helmet
        htmlAttributes={
          {lang: `en`}
        }

        title='Africa Tech Events | Africa tech space | Technology in Africa | Coding Africa, afro tech | black tech conferences, Black Startup, tech hubs africa'
        meta={[
          { 
            name: 'description',
            content: `The place to discover Africa Tech Events and Startup Conferences happening in Africa and in the African diaspora.
            We connect Africa Tech Community in the Mainland and in the Diaspora by curating every year the top events happening
            across the globe in the afro tech space. Coding events, startup competitions, technology conferences and more.`
          },
          { 
            name: 'keywords',
            content: `Africa, Africa tech, africa tech news, Technology in Africa, IT news africa, africa technology news, Disrupt africa, africa tech events, africa coders, Caribbean tech space, Haiti tech, Haiti conferences, black conferences,
             afrotech, africa tech space, startup africa, black startup, black tech, technology in africa, tech hub, africa news, tech summit,
             google africa, facebook africa, coding africa, conferences in africa, tech conferences in africa, IT Conferences, 
             upcoming events in south africa, innovaton summit, africa women, tech women, africa conferences, africa tech connect, connect africa,
              tech square, africa tech square, africa technology, africans tech, africa innovations, africa conferences, afrotech, afro, black tech,
             black in tech`
          }
        ]}
        links={
          [
            {
              rel:'canonical',
              href:'https://www.africatechsquare.com/' 
            },
            {
              rel:'shortcut icon',
              href:'https://www.africatechsquare.com/favicon.ico' 
            },
          ]
        }
      />
      <CustomNavBar title={data.site.siteMetadata.title}></CustomNavBar>
        <InstantSearch
          appId={ appId }
          apiKey={ apiKey }
          indexName={ indexName }
        >
          {/* Search widgets will go there */
            <div>
              <AppStyles>
                <div className="content">
                  {children()}
                </div>
              </AppStyles>
              <Footer></Footer>
            </div>
          }
      </InstantSearch>
    </div>
  </IntlProvider>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper

export const query = graphql`
  query LayoutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
