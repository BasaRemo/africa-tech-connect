import React from "react"
import styled from 'styled-components'

const AboutStyles = styled.div`
  padding: 1rem;
  margin: 3rem auto;
  max-width: 980px;
  margin-bottom: 20%;
  
  p {
    font-weight: semi-bold;
  }
`

export default ({ data }) => (
  <AboutStyles>
    <h1>
      About {data.site.siteMetadata.title}
    </h1>
    <h2> Community </h2>
    <p>
      The goal of {data.site.siteMetadata.title} is to connect African Tech community on the Mainland and in the Diaspora.
      We select the top events happening all over the world in the Afro-Tech community and will present live social media updates
      from participants during the entire 2018 season. 
    </p>

    <h2> Resources </h2>
    <p>
      We also want to be the place where startup founders to find inspiration and resources available to them to start and scale their business.
      You will find information about competitions, Incubators, Hackathons and the must attend tech events outside the community all over the world.
    </p>
  </AboutStyles>
)

export const query = graphql`
  query AboutQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`