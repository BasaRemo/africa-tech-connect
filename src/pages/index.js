import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import SEO from '../Components/SEO.js'
import Hero from '../Components/Hero.js'
import SocialFeed from '../Components/SocialFeed.js'
import Headlines from '../Components/Headlines.js'
// import { Offline, Online } from 'react-detect-offline';
import SearchResult from '../Components/SearchResult.js';
import CategoryMenu from '../Components/CategoryMenu.js';

import EventForm from '../Components/EventForm.js'

const HomeStyles = styled.div`
  .home-content {
    padding: 0 1rem;
    margin: 3rem auto;
    max-width: 1100px;
  }

  .offline-message {
    font-size: 15px;
    font-weight: semi-bold;
    font-size: calc(100% + 0.5vw);
  }

  .comments {
      padding-top: 30px;
      max-width: 600px;
  }
  
  .heading-subtitle {
      font-size: 0.9rem;
  }
`

export default ({ data }) => {

  const featuredEvent = data.allContentfulEvent.edges[0].node

  return (
    <HomeStyles>
      <Hero event={featuredEvent}></Hero>
      <div className="home-content">
        <div className="uk-flex-center uk-grid-medium" data-uk-grid="true">
          <CategoryMenu className="uk-child-width-1-1@s uk-width-auto@m"></CategoryMenu>
          <SearchResult className="uk-child-width-1-1@s uk-width-expand@m"></SearchResult>
          <SocialFeed data={data} className="uk-child-width-1-1@s uk-width-1-3@m"></SocialFeed>
        </div>
      </div>
    </HomeStyles>
  );
}


export const query = graphql`
  query FeedsQuery {
    allContentfulEvent(sort: {fields: [startDate], order: ASC}) {
      edges {
        node {
          title
          whyAttend {
            whyAttend
          }
          startDate(formatString: "MMMM DD, YYYY")
          countryIso
          address
          url
          id
        }
      }
      totalCount
    }
  }
`
