import React, { Component} from 'react';
import FacebookProvider, { EmbeddedPost } from 'react-facebook';

import styled from 'styled-components'

import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'


const LiveUpdateStyles = styled.div`
    .clean {
        background-color: #CF3060;
    }

    padding-top: 50px;
    margin-bottom: 20px;

    h4 {
        margin-bottom:5px;
        font-weight: 400;
        text-align: left;
    }

    h3 {
        font-weight: 500;
        text-align: left;
    }

    .story-subtitle {
        text-align: left;
    }

    .comment {
        margin-top: 10px;
        margin-bottom: 10px;
        text-align: left;
        font-size: 0.9rem;
    }

    .descr {
        uk-text-leftalign: left;
        font-size:15px;
        font-weight: semi-bold;
    }

    .title-post {
        padding-top: 15px;
        color: #333;
    }

    .facebook-post {
        margin-right: 10px;
    }

    .ats-heading {
        text-align: center;
        margin-bottom: 0px;
    }

    .heading-title {
        font-size: 2rem;
    }

    .heading-subtitle {
        font-size: 0.9rem;
        color: #333;
    }

    .event-url {
        color: #333;
    }
`
export default ({featuredEvent}) => (
  <LiveUpdateStyles>
    <h2 className="ats-heading heading-title"><a className="event-url" href={featuredEvent.url} target="_blank" rel="noopener">{featuredEvent.title}</a></h2>
    <div className="ats-heading heading-subtitle">Social media updates from the event happening now</div>
    <hr></hr>
    <ul className="uk-grid-small uk-child-width-1-2@s uk-child-width-1-2@m uk-child-width-1-2@l uk-text-center" data-uk-grid="true">
        <li >
            <div className="uk-heading uk-text-left title-post"> The Ansut caravan will go over 5 city in Ivory coast teaching about how to take advantage of the internet in communities.</div>
            <div className="comment">They will teach people about blogging, social media and more</div>
            <FacebookProvider className="facebook-post" appId="816134778490827">
                <EmbeddedPost href="https://www.facebook.com/ansutblogcamp/photos/a.202258200341247.1073741829.196223350944732/203151140251953/" width="500" />
            </FacebookProvider>
        </li>
        <li>
            <br></br>
            <div className="uk-heading uk-text-left title-post">100 computers waiting for Beoumi participants  </div>
            <div className="comment">Day 5 of the ansut Blog Camp</div>
            <FacebookProvider appId="816134778490827" className="facebook-post">
                <EmbeddedPost href="https://www.facebook.com/ansutblogcamp/photos/a.202258200341247.1073741829.196223350944732/206004553299945" width="500" />
            </FacebookProvider>
        </li>
    </ul>
  </LiveUpdateStyles>
)
