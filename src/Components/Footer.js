import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

import logoSmall from '../images/logo-main.png'

const FooterStyles = styled.div`
  background: #2E2532;
  height: 100px;
  .custom-nav-container {
  	max-width: 980px;
  }

  .title {
    color: #F8F8F8;
    padding: 0.3rem;
    font-size: calc(100% + 0.1vw);
  }

  .copyright {
    color: #F8F8F8;
    font-size: calc(90% + 0.05vw);
  }

  padding-top: 40px;
`

export default ({ title }) => (
	<FooterStyles className="uk-flex uk-flex-center">
		<div className="footer-container uk-flex-column uk-flex uk-flex-middle">
        <span className="title">Made with ❤️ by Basa</span>
        <div className="copyright">Copyright ©2017 AfricaTechSquare. All rights reserved.</div>
		</div>
	</FooterStyles>
)