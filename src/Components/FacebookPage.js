import React, { Component} from 'react';
import FacebookProvider, { Page } from 'react-facebook';

export default class FacebookPage extends Component {
  render() {
    return (
      <FacebookProvider appId="816134778490827">
        <Page href="fb.me/africatechsquare" tabs="timeline" />
      </FacebookProvider>    
    );
  }
}