import React from 'react'
import styled from 'styled-components'
import {Panel, InfiniteHits, Menu, RefinementList, MultiRange} from 'react-instantsearch/dom';

import orderBy from 'lodash/orderBy';
import forEach from 'lodash/forEach';

const countries = require("i18n-iso-countries");
countries.registerLocale(require("i18n-iso-countries/langs/en.json"));

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

const CategoryStyles = styled.div`
    max-width: 245px;  

    .form_box {
        padding: 20px;
        background: white;
        margin-bottom: 20px;
        min-height: 410px;
    }

    .ais-RefinementList__itemCount {
       color: black;
        margin-bottom: 6px;
        float: right;
    }

    label {
        font-size: 12px;
        color: black;
        font-weight: 400;
    }

    .title {
        font-size: 22px;
        font-weight: 400;
    }

    .ais-RefinementList__showMore {
        font-size: 14px;
        font-weight: 600;
        margin-top: 15px;
        color: #1D75DC;
    }

    @media all and (min-width: 620px) {
        padding-top: 56px;
        min-width: 240px;
    }
`
export default () => (
  <CategoryStyles>
        <div className="uk-card form_box">
            <div className="title"> Countries </div>
              <RefinementList
                attributeName="countryIso"
                limitMax={20}
                limitMin={10}
                showMore={true}
                transformItems={items =>
                    orderBy(forEach(items, item => item.label = countries.getName(item.label, "en")), ['count', 'label'], ['desc', 'asc'])
                }
              />
        </div>

        <div className="uk-card form_box">
            <div className="title"> 2018 Calendar </div>
              <RefinementList
                attributeName="month"
                limitMax={12}
                limitMin={12}
                showMore={false}
                transformItems={items =>
                    orderBy(forEach(items, item => {
                        item.number = parseInt(item.label);
                        item.label = monthNames[item.label];
                        return item;
                    }), ['number'], ['asc'])
                }
              />
        </div>

  </CategoryStyles>
)