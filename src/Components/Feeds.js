import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'

import UIkit from 'uikit';
import groupBy from 'lodash/groupBy';

import EventCard from '../Components/EventCard.js'

const FeedsStyles = styled.div`
    
    width: 560px;
    .clean {
        background-color: #CF3060;
        color: white;
    }

    .upcoming {
        padding-top: 50px;
        margin-bottom: 0px;
    }

    h2 {
        padding-top: 30px;
    }

    .heading-title {
        font-size: 2rem;
        color: #333;
    }

    .heading-subtitle {
        font-size: 0.9rem;
    }

    .month-heading {
        font-size: 1.5rem;
        color: #333;
        margin-bottom: 0px;
        margin-top: 40px;
    }

    hr {
        margin-top: 5px;
    }
`

export default ({ data }) => {
    const groupByMonth = groupBy(data.allContentfulEvent.edges, function(item) {
      return item.node.startDate.substring(0,3); 
    });
    return (
        <FeedsStyles className="content">
            
            <h2 className="heading-title upcoming">Upcoming Events </h2>
            <div className="heading-subtitle">All major afro tech events happening accross the globe</div>
            {
                Object.keys(groupByMonth).map(key =>
                    <div key={key}>
                        <div className="month-heading">{key}</div>
                        <hr></hr>
                        <ul className="uk-grid-small uk-child-width-1-1@s uk-child-width-1-1@m uk-child-width-1-1@l uk-text-center" data-uk-grid="true">
                            { groupByMonth[key].map(({ node }) => (
                                <li key={node.id}>
                                    <EventCard event={node} ></EventCard>
                                </li>
                            )) }
                        </ul>
                    </div>
                )
            }

        </FeedsStyles>
    );
        
};

