import React from 'react'
import styled from 'styled-components'

import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'

const SocialStyles = styled.div`
    .clean {
        background-color: #CF3060;
    }

     h4 {
        margin-bottom:5px;
        font-weight: 500;
    }

    h3 {
        font-weight: 500;
    }

    @media all and (min-width: 620px) {
        padding-top: 50px;
    }

    .story-subtitle {
        text-align: left;
    }

    .ats-heading {
        text-align: center;
        margin-bottom: 0px;
        
    }

    .title {
        font-size: 2rem;
        color: #333;
    }

    .story-title {
        font-size: 1.0rem;
        color: #333;
    }

    .heading-subtitle {
        font-size: 0.9rem;
        color: #333;
    }
`
export default ({data}) => (
  <SocialStyles>
    <h2 className="ats-heading title">Cover Stories</h2>
    <div className="ats-heading heading-subtitle">Highlights from the Afro tech community around the world</div>
    <hr></hr>
    <ul className="uk-grid-small uk-child-width-1-1@s uk-text-center" data-uk-grid="true">
        <li>
            <div className="story-title"> The deadline is March 1st, 2018</div>
            <TweetEmbed id='948558179369091072' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="story-title">Discovery the best tech from Africans </div>
            <TweetEmbed id='946063657963786240' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="story-title"> Google's Launchpad regional initiatives</div>
            <TweetEmbed id='950638300385173504' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="story-title">Building health tech for Africa </div>
            <TweetEmbed id='950861752610230274' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="story-title">Must read if you want to launch in Africa </div>
            <TweetEmbed id='948499625660616704' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="story-title">The Afro Generation of Builders</div>
            <TweetEmbed id='946431912557600769' options={{cards: 'hidden' }}/>
        </li>
    </ul>
  </SocialStyles>
)
