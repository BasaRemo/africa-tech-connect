import React from 'react'
import styled from 'styled-components'

import EventCard from '../Components/EventCard.js'
import {InfiniteHits, SearchBox, RefinementList, ScrollTo} from 'react-instantsearch/dom';

const HeroStyles = styled.div`
  background: #1656A0;
  padding: 70px 20px 40px 20px; 
  margin-top: -38px;

  @media screen and (max-width:450px) {
       padding-top: 70px;
  }

  .main-title {
    font-size: 30px;
    font-weight: bold;
    font-size: calc(100% + 3vw);
    color: #FFBA49;
  }

  .main-subtitle {
    font-size: 20px;
    font-weight: bold;
    font-size: calc(100% + 1vw);
    padding-bottom: 4rem;
    margin-top: 5px;
    max-width: 800px;
  }

  .copy {
    color: white;
  }

  .annonce {
    text-align: center;
    font-size: 15px;
    font-weight: semi-bold;
    font-size: calc(100% + 0.5vw);
  }

  .ais-SearchBox__input {
    text-align: center;
    min-width: 300px;
  }

  .search-menu {
    text-align: center;
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .search-container {
    justify-content: center;
  }

  @media all and (min-width: 620px) {
    .search-menu {
      min-width: 475px;
    } 

    .main-title {
      text-align: center;
      font-size: calc(100% + 2vw);
    }

    .main-subtitle {
      text-align: center;
      max-width: 700px;
      font-size: calc(100% + 0.8vw);
    }
  }
`

export default ({ event }) => (
  <HeroStyles className="header-image">
    <div className="uk-flex uk-flex-center">
      <div className="uk-flex uk-flex-column">
        <h1 className="main-title"> Where African Tech Connects</h1>
        <h2 className="copy main-subtitle"> Discover the Best Tech Events happening accross Africa and in the Diaspora</h2>
        <span className="copy annonce"> Browse events </span>
        <div className="uk-flex uk-flex-center search-container">
          <div className="search-menu">
            <SearchBox  />
          </div>
        </div>
      </div>
    </div>
  </HeroStyles>
)
