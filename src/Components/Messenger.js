import React, { Component} from 'react';
import FacebookProvider, { MessageUs } from 'react-facebook';

export default class Messenger extends Component {
  render() {
    return (
      <FacebookProvider appId="816134778490827">
        <MessageUs appId="816134778490827" pageId="795204320682461"/>
      </FacebookProvider>    
    );
  }
}