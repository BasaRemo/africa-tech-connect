import React from 'react'
import styled from 'styled-components'

const EventFormStyles = styled.div`
    max-width: 350px;  
    margin-top: 13px;

    .form-box {
        padding: 20px;
        background: #FFFFFF;
    }

    .form-button {
        background: #DA5430;
        color: white;
        font-weight: 400;
    }

    .form-legend {
        color: black;
    }

    textarea {
        resize: none;
    }

    .gotcha {
        display:none;
    }
`
export default () => (
  <EventFormStyles>
        <form className="uk-card form-box" action="https://formspree.io/africatechconnect@gmail.com" method="POST">
            <fieldset className="uk-fieldset">
                <legend className="uk-legend form-legend">Propose an Event</legend>
                <div className="uk-margin">
                    <input className="uk-input" type="text" name="name" placeholder="your name"></input>
                </div>
                <div className="uk-margin">
                    <input className="uk-input" type="email" name="_replyto" placeholder="your email"></input>
                </div>
                <div className="uk-margin">
                    <input className="uk-input" type="text" name="event_url" placeholder="the event url"></input>
                </div>
                <div className="uk-margin">
                    <textarea className="uk-textarea" rows="5" name="message" placeholder="Tell us a little bit about this event"></textarea>
                </div>
                <div className="uk-margin">
                    <input className="uk-button uk-button-default uk-width-1-1 uk-margin-small-bottom form-button" type="submit" value="Send"></input>
                </div>
                <input type="hidden" name="_next" value="https://www.africatechsquare.com"></input>
                <input type="hidden" name="_subject" value="New tech event proposal"></input>
                <input className="gotcha" type="text" name="_gotcha"></input>
            </fieldset>
        </form>
  </EventFormStyles>
)