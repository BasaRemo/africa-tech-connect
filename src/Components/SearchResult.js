import React from 'react';

import {InfiniteHits, SearchBox, RefinementList, ScrollTo} from 'react-instantsearch/dom';
import Event from '../Components/Event.js'
import styled from 'styled-components'
import UIkit from 'uikit';
import orderBy from 'lodash/orderBy';

const SearchResultStyles = styled.div`

  h2 {
    padding-top: 20px;
    margin-top: 0px;
  }

  .heading-title {
    padding-top: 10px;
    font-size: 2rem;
    color: #333;
    margin-bottom: 5px;
  }

  .heading-subtitle {
    font-size: 0.9rem;
    padding-left: 20px;
  }

  .ais-SearchBox__root {
  	margin: 20px;
    width: 250px;
  }

  .search-menu {
    margin-top: 10px;
    margin-bottom: 10px;
    background: #FFFFFF;
  }

  .ais-InfiniteHits__loadMore {
  	margin-top: 15px;
  	margin-bottom: 15px;
  	padding: 15px;
  	background: #2569B8;
  	color: white;
  	font-size: 15px;
  }

  @media all and (min-width: 620px) {
    min-width: 475px;
  }
`

export default ({ nodes }) => {
  return (
    <SearchResultStyles className="container">
      <h2 className="heading-title">Upcoming Events </h2>
      <InfiniteHits className="uk-grid-small uk-child-width-1-1" data-uk-grid="true" 
        hitComponent={Event}
      />
    </SearchResultStyles>
  );
};