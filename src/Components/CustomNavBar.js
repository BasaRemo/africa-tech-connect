import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

import logoSmall from '../images/logo-main.png'

const NavBarStyles = styled.div`
  background: #1656A0;

  nav {
    width: 92%;
    background: #1656A0;
    a {
      color: white;
      padding: 1rem;
      display: inline-block;
    }
  }

  .custom-nav-container {
  	max-width: 980px;
  	margin: 0 auto 0 auto;
  	padding: 0 1rem;
  }

  .logo {
  	margin-top: 15px;
  	margin-bottom: 15px;
  	width: 44px;
    vertical-align: none;
  }

  .navbar-right {
  	float: right;
  	margin-top: 10px;
  }

  .title {
    color: white;
    padding: 1rem;
    font-weight: bold;
  }

  @media all and (max-width: 479px) {
    .title {display:none;}
  }
`

export default ({ title }) => (
	<NavBarStyles id="nav-bar-content" data-uk-sticky="true">
		<nav className="custom-nav-container">
		    <span className="navbar-left">
		    	<img className="logo" src={logoSmall} alt={"logoSmall"}/>
          <span className="title">{title}</span>
		    </span>
		    <div className="navbar-right">
		      <Link to="/">Home</Link>
	        <Link to="/about">About</Link>
		    </div>
		</nav>
	</NavBarStyles>
)
