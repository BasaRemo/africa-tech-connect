import React from 'react';

import {InstantSearch} from 'react-instantsearch/dom';
import SearchResult from '../Components/SearchResult.js';
const appId = 'YN6ZB5YGKP'//process.env.ALGOLIA_APP_ID || ''
const apiKey = 'e712af0f452796b5b08b38429443a49d'//process.env.ALGOLIA_API_KEY || ''
const indexName = 'africatechconnect'//process.env.ALGOLIA_INDEX_NAME || ''

export default ({ nodes }) => {
  return (
	  <InstantSearch
	    appId={ appId }
	    apiKey={ apiKey }
	    indexName={ indexName }
	  >
	    {/* Search widgets will go there */}
	    <SearchResult/>
	  </InstantSearch>
  )
};
