import React from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import ToggleDisplay from 'react-toggle-display';

import FlagIcon from './FlagIcon.js'

import {IntlProvider, FormattedDate} from 'react-intl';

const EventCardStyles = styled.div`

  max-width: 580px;
  margin-bottom: 10px; 

  .clean {
    background-color: #CF3060;
    color: white;
  }

  .text {
    font-family: Arial;
    text-align: left;
    color: #3A3A3A; 
  }

  .title {
    font-family: Arial;
    font-size: 1.2rem;
    line-height: 1.2;
    font-weight: 500;
    color: #1D75DC; 
  }

  p {
    font-family: Arial;
    color: #3A3A3A;
  }

  .details {
    padding: 1rem 1rem 0  1rem;
  }

  .why {
    font-weight: 600;
    font-size: 1.1rem;
  }

  .flag-icon {
    float: right;
    padding-top: 40px;
  }

  @media all and (min-width: 620px) {
      min-width: 475px;
  }
`

const LocationDivStyled = styled.div`
    color: black;
    font-weight: normal;

`

const DateDivStyled = styled.div`
    color: black; 
    font-weight: bold;
    line-height: 1.2;
`

const BottomStyledDiv = styled.div`
   padding: 1rem;
`

export default ({ event }) => {
  if (event.countryIso == null) {
    return (null);
  }
  return (
    <EventCardStyles className="uk-card uk-card-default uk-box-shadow-small">
      <div className="details">
          <a className="title text" style={{display: "table-cell"}} href={event.url} target="_blank" rel="noopener">{event.title}</a>
      </div>
      <BottomStyledDiv>
        <FlagIcon code={event.countryIso} size="lg" />
        <div className="uk-flex uk-flex-column text">
            <LocationDivStyled>{event.address}</LocationDivStyled>
            <DateDivStyled>
              <FormattedDate
                  value={event.startDate}
                  day="numeric"
                  month="long"
                  year="numeric" />
            </DateDivStyled>
        </div>
      </BottomStyledDiv>
    </EventCardStyles>
  )
};