import React from 'react'
import styled from 'styled-components'

import FacebookPage from '../Components/FacebookPage.js'
import EventForm from '../Components/EventForm.js'
import Headlines from '../Components/Headlines.js'
import InstagramEmbed from 'react-instagram-embed'
import TweetEmbed from 'react-tweet-embed'

const SocialStyles = styled.div`
    max-width: 310px;   

    h4 {
        margin-bottom:5px;
        font-weight: 500;
    }

    @media all and (min-width: 620px) {
        padding-top: 44px;
    }

    .subtitle {
        text-align: left;
        padding-bottom: 5px;
    }
    
    .heading-title {
        font-size: 2rem;
        color: #333;
    }

    .heading-subtitle {
        font-size: 0.9rem;
        color: #333;
    }

    hr {
        margin-top: 10px;
    }
`
export default ({data}) => (
  <SocialStyles>
    <ul className="uk-grid-medium uk-child-width-1-1@s uk-text-center" data-uk-grid="true">
        <li>
            <EventForm></EventForm>
        </li>
        <li>
            <div className="uk-heading uk-text-left"> The deadline is March 1st, 2018</div>
            <TweetEmbed id='948558179369091072' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="uk-heading uk-text-left">Discover the best tech built by Africans </div>
            <TweetEmbed id='946063657963786240' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <div className="uk-heading uk-text-left"> Google's Launchpad regional initiatives</div>
            <TweetEmbed id='950638300385173504' options={{cards: 'hidden' }}/>
        </li>
        <li>
            <h4 className="uk-heading uk-text-left">Like Our Tech Safari on Facebook! ⛵</h4>
            <div className="subtitle">We share our progress building this project and the amazing stuffs happening in the Afro Tech</div>
            <FacebookPage></FacebookPage>
        </li>
    </ul>
  </SocialStyles>
)
